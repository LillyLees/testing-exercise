import pytest


from main import *
def fib(n):
    s = 0
    f = 1
    while n > 0:
        temp = s + f
        s = f
        f = temp
        n = n - 1
    return (s)


def encrypt(shift_val, message):
    if message.strip().isalpha() == False:
        return("ERROR: please only use alphabetic charicters and spaces")
    encrpmess = ""
    for i in list(message):
        if( str(i) != " "):
            encrpmess += str(chr(ord(i) + shift_val))
        else:
            encrpmess += " "
    return(encrpmess)


def decrypt(shift_val, message):
    if message.strip().isalpha() == False:
        return("ERROR: please only use alphabetic charicters and spaces")
    encrpmess = ""
    for i in list(message):
        if( str(i) != " "):
            encrpmess += str(chr(ord(i) - shift_val))
        else:
            encrpmess += " "
    return(encrpmess)
